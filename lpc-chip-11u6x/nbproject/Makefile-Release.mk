#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/adc_11u6x.o \
	${OBJECTDIR}/src/chip_11u6x.o \
	${OBJECTDIR}/src/clock_11u6x.o \
	${OBJECTDIR}/src/crc_11u6x.o \
	${OBJECTDIR}/src/dma_11u6x.o \
	${OBJECTDIR}/src/gpio_11u6x.o \
	${OBJECTDIR}/src/gpiogroup_11u6x.o \
	${OBJECTDIR}/src/i2c_11u6x.o \
	${OBJECTDIR}/src/i2cm_11u6x.o \
	${OBJECTDIR}/src/iocon_11u6x.o \
	${OBJECTDIR}/src/pinint_11u6x.o \
	${OBJECTDIR}/src/pmu_11u6x.o \
	${OBJECTDIR}/src/ring_buffer.o \
	${OBJECTDIR}/src/romdiv_11u6x.o \
	${OBJECTDIR}/src/rtc_11u6x.o \
	${OBJECTDIR}/src/sct_11u6x.o \
	${OBJECTDIR}/src/ssp_11u6x.o \
	${OBJECTDIR}/src/stopwatch_11u6.o \
	${OBJECTDIR}/src/syscon_11u6x.o \
	${OBJECTDIR}/src/sysinit_11u6x.o \
	${OBJECTDIR}/src/timer_11u6x.o \
	${OBJECTDIR}/src/uart_0_11u6x.o \
	${OBJECTDIR}/src/uart_n_11u6x.o \
	${OBJECTDIR}/src/wwdt_11u6x.o


# C Compiler Flags
CFLAGS=-fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a

${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a

${OBJECTDIR}/src/adc_11u6x.o: src/adc_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/adc_11u6x.o src/adc_11u6x.c

${OBJECTDIR}/src/chip_11u6x.o: src/chip_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/chip_11u6x.o src/chip_11u6x.c

${OBJECTDIR}/src/clock_11u6x.o: src/clock_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/clock_11u6x.o src/clock_11u6x.c

${OBJECTDIR}/src/crc_11u6x.o: src/crc_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/crc_11u6x.o src/crc_11u6x.c

${OBJECTDIR}/src/dma_11u6x.o: src/dma_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/dma_11u6x.o src/dma_11u6x.c

${OBJECTDIR}/src/gpio_11u6x.o: src/gpio_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gpio_11u6x.o src/gpio_11u6x.c

${OBJECTDIR}/src/gpiogroup_11u6x.o: src/gpiogroup_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gpiogroup_11u6x.o src/gpiogroup_11u6x.c

${OBJECTDIR}/src/i2c_11u6x.o: src/i2c_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/i2c_11u6x.o src/i2c_11u6x.c

${OBJECTDIR}/src/i2cm_11u6x.o: src/i2cm_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/i2cm_11u6x.o src/i2cm_11u6x.c

${OBJECTDIR}/src/iocon_11u6x.o: src/iocon_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/iocon_11u6x.o src/iocon_11u6x.c

${OBJECTDIR}/src/pinint_11u6x.o: src/pinint_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/pinint_11u6x.o src/pinint_11u6x.c

${OBJECTDIR}/src/pmu_11u6x.o: src/pmu_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/pmu_11u6x.o src/pmu_11u6x.c

${OBJECTDIR}/src/ring_buffer.o: src/ring_buffer.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ring_buffer.o src/ring_buffer.c

${OBJECTDIR}/src/romdiv_11u6x.o: src/romdiv_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/romdiv_11u6x.o src/romdiv_11u6x.c

${OBJECTDIR}/src/rtc_11u6x.o: src/rtc_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/rtc_11u6x.o src/rtc_11u6x.c

${OBJECTDIR}/src/sct_11u6x.o: src/sct_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sct_11u6x.o src/sct_11u6x.c

${OBJECTDIR}/src/ssp_11u6x.o: src/ssp_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ssp_11u6x.o src/ssp_11u6x.c

${OBJECTDIR}/src/stopwatch_11u6.o: src/stopwatch_11u6.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/stopwatch_11u6.o src/stopwatch_11u6.c

${OBJECTDIR}/src/syscon_11u6x.o: src/syscon_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/syscon_11u6x.o src/syscon_11u6x.c

${OBJECTDIR}/src/sysinit_11u6x.o: src/sysinit_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sysinit_11u6x.o src/sysinit_11u6x.c

${OBJECTDIR}/src/timer_11u6x.o: src/timer_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/timer_11u6x.o src/timer_11u6x.c

${OBJECTDIR}/src/uart_0_11u6x.o: src/uart_0_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/uart_0_11u6x.o src/uart_0_11u6x.c

${OBJECTDIR}/src/uart_n_11u6x.o: src/uart_n_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/uart_n_11u6x.o src/uart_n_11u6x.c

${OBJECTDIR}/src/wwdt_11u6x.o: src/wwdt_11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -Wall -DCORE_M0PLUS -DNDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/wwdt_11u6x.o src/wwdt_11u6x.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6x.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
