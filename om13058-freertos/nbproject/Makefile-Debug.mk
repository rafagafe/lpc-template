#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1034102186/FreeRTOSCommonHooks.o \
	${OBJECTDIR}/_ext/1034102186/heap_3.o \
	${OBJECTDIR}/_ext/1034102186/list.o \
	${OBJECTDIR}/_ext/1034102186/port.o \
	${OBJECTDIR}/_ext/1034102186/queue.o \
	${OBJECTDIR}/_ext/1034102186/tasks.o \
	${OBJECTDIR}/src/aeabi_romdiv_patch.o \
	${OBJECTDIR}/src/cr_startup_lpc11u6x.o \
	${OBJECTDIR}/src/freertos_blinky.o \
	${OBJECTDIR}/src/sysinit.o


# C Compiler Flags
CFLAGS=-fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../lpc-board-nxp-lpcxpresso-11u68/dist/Debug/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a ../lpc-chip-11u6x/dist/Debug/liblpc-chip-11u6x.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/freertos.elf

${CND_DISTDIR}/${CND_CONF}/freertos.elf: ../lpc-board-nxp-lpcxpresso-11u68/dist/Debug/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a

${CND_DISTDIR}/${CND_CONF}/freertos.elf: ../lpc-chip-11u6x/dist/Debug/liblpc-chip-11u6x.a

${CND_DISTDIR}/${CND_CONF}/freertos.elf: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/freertos.elf ${OBJECTFILES} ${LDLIBSOPTIONS} -Xlinker --gc-sections -Xlinker --allow-multiple-definition -mcpu=cortex-m0 -mthumb -T src/LPC11U68.ld -lgcc -lc -lm --specs=rdimon.specs

${OBJECTDIR}/_ext/1034102186/FreeRTOSCommonHooks.o: ../freertos/src/FreeRTOSCommonHooks.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/FreeRTOSCommonHooks.o ../freertos/src/FreeRTOSCommonHooks.c

${OBJECTDIR}/_ext/1034102186/heap_3.o: ../freertos/src/heap_3.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/heap_3.o ../freertos/src/heap_3.c

${OBJECTDIR}/_ext/1034102186/list.o: ../freertos/src/list.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/list.o ../freertos/src/list.c

${OBJECTDIR}/_ext/1034102186/port.o: ../freertos/src/port.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/port.o ../freertos/src/port.c

${OBJECTDIR}/_ext/1034102186/queue.o: ../freertos/src/queue.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/queue.o ../freertos/src/queue.c

${OBJECTDIR}/_ext/1034102186/tasks.o: ../freertos/src/tasks.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1034102186
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1034102186/tasks.o ../freertos/src/tasks.c

${OBJECTDIR}/src/aeabi_romdiv_patch.o: src/aeabi_romdiv_patch.s 
	${MKDIR} -p ${OBJECTDIR}/src
	$(AS) $(ASFLAGS) -g -o ${OBJECTDIR}/src/aeabi_romdiv_patch.o src/aeabi_romdiv_patch.s

${OBJECTDIR}/src/cr_startup_lpc11u6x.o: src/cr_startup_lpc11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/cr_startup_lpc11u6x.o src/cr_startup_lpc11u6x.c

${OBJECTDIR}/src/freertos_blinky.o: src/freertos_blinky.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/freertos_blinky.o src/freertos_blinky.c

${OBJECTDIR}/src/sysinit.o: src/sysinit.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -Iinc -I../freertos/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sysinit.o src/sysinit.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/freertos.elf

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
