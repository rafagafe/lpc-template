#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=freertos.elf
CND_ARTIFACT_PATH_Debug=dist/Debug/freertos.elf
CND_PACKAGE_DIR_Debug=dist/Debug/arm-none-eabi-Linux-x86/package
CND_PACKAGE_NAME_Debug=om13058-freertos.tar
CND_PACKAGE_PATH_Debug=dist/Debug/arm-none-eabi-Linux-x86/package/om13058-freertos.tar
# Release configuration
CND_PLATFORM_Release=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release
CND_ARTIFACT_NAME_Release=freertos.elf
CND_ARTIFACT_PATH_Release=dist/Release/freertos.elf
CND_PACKAGE_DIR_Release=dist/Release/arm-none-eabi-Linux-x86/package
CND_PACKAGE_NAME_Release=om13058-freertos.tar
CND_PACKAGE_PATH_Release=dist/Release/arm-none-eabi-Linux-x86/package/om13058-freertos.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
