#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/aeabi_romdiv_patch.o \
	${OBJECTDIR}/src/cr_startup_lpc11u6x.o \
	${OBJECTDIR}/src/sysinit.o \
	${OBJECTDIR}/src/systick.o


# C Compiler Flags
CFLAGS=-fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../lpc-board-nxp-lpcxpresso-11u68/dist/Debug/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a ../lpc-chip-11u6x/dist/Debug/liblpc-chip-11u6x.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/periph-systick.elf

${CND_DISTDIR}/${CND_CONF}/periph-systick.elf: ../lpc-board-nxp-lpcxpresso-11u68/dist/Debug/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a

${CND_DISTDIR}/${CND_CONF}/periph-systick.elf: ../lpc-chip-11u6x/dist/Debug/liblpc-chip-11u6x.a

${CND_DISTDIR}/${CND_CONF}/periph-systick.elf: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/periph-systick.elf ${OBJECTFILES} ${LDLIBSOPTIONS} -Xlinker --gc-sections -Xlinker --allow-multiple-definition -mcpu=cortex-m0 -mthumb -T src/LPC11U68.ld

${OBJECTDIR}/src/aeabi_romdiv_patch.o: src/aeabi_romdiv_patch.s 
	${MKDIR} -p ${OBJECTDIR}/src
	$(AS) $(ASFLAGS) -g -o ${OBJECTDIR}/src/aeabi_romdiv_patch.o src/aeabi_romdiv_patch.s

${OBJECTDIR}/src/cr_startup_lpc11u6x.o: src/cr_startup_lpc11u6x.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/cr_startup_lpc11u6x.o src/cr_startup_lpc11u6x.c

${OBJECTDIR}/src/sysinit.o: src/sysinit.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sysinit.o src/sysinit.c

${OBJECTDIR}/src/systick.o: src/systick.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Isrc -I../lpc-chip-11u6x/inc -I../lpc-board-nxp-lpcxpresso-11u68/inc -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/systick.o src/systick.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/periph-systick.elf

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
