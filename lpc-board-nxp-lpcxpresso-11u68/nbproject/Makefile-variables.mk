#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a
CND_ARTIFACT_PATH_Debug=dist/Debug/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a
CND_PACKAGE_DIR_Debug=dist/Debug/arm-none-eabi-Linux-x86/package
CND_PACKAGE_NAME_Debug=lpc-board-nxp-lpcxpresso-11u68.tar
CND_PACKAGE_PATH_Debug=dist/Debug/arm-none-eabi-Linux-x86/package/lpc-board-nxp-lpcxpresso-11u68.tar
# Release configuration
CND_PLATFORM_Release=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release
CND_ARTIFACT_NAME_Release=liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a
CND_ARTIFACT_PATH_Release=dist/Release/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a
CND_PACKAGE_DIR_Release=dist/Release/arm-none-eabi-Linux-x86/package
CND_PACKAGE_NAME_Release=lpc-board-nxp-lpcxpresso-11u68.tar
CND_PACKAGE_PATH_Release=dist/Release/arm-none-eabi-Linux-x86/package/lpc-board-nxp-lpcxpresso-11u68.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
