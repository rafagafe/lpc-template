#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/board.o \
	${OBJECTDIR}/src/board_sysinit.o


# C Compiler Flags
CFLAGS=-fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a

${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a

${OBJECTDIR}/src/board.o: src/board.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -I../lpc-chip-11u6x/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/board.o src/board.c

${OBJECTDIR}/src/board_sysinit.o: src/board_sysinit.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -Wall -DCORE_M0PLUS -DDEBUG -D__USE_LPCOPEN -D__USE_ROMDIVIDE -Iinc -I../lpc-chip-11u6x/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/board_sysinit.o src/board_sysinit.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/liblpc-chip-11u6xlpc-board-nxp-lpcxpresso-11u68.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
